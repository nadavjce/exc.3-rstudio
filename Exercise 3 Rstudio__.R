#1)

Roundtask<-function(m,s){
  task<-ceiling(rnorm(1,m,s))
  return(task)
}

Roundtask(10,2)

#2)
project.length<-function(x){
  A<-Roundtask(10,3)
  B<-Roundtask(10,3)
  C<-5
  D<-10
  E<-Roundtask(5,2)
  F<-5
  chain.1<-A+B+C+F
  chain.2<-A+D++E+F
  if(chain.1>chain.2){
    project.time=chain.1
    CP="ABCF"
  }else{
    project.time=chain.2
    CP="ADEF"
  }
  V<-c(project.time,CP)
  return(V)
}

project.length()

#3)

samples<-1:10000
project.time.vec <- sapply(samples,project.length)
project.time.vec.Duration<-as.integer(project.time.vec)
project.time.vec.Duration<-project.time.vec.Duration[!is.na(project.time.vec.Duration)]
hist(project.time.vec.Duration,100)

#4)

samples<-1:10000
project.time.vec <- sapply(samples,project.length)
project.time.vec.Probebility<-as.factor(project.time.vec[2,])
summary.factor(project.time.vec.Probebility)
probebility<-summary.factor(project.time.vec.Probebility)
Probebility<-as.integer(probebility)
probebility<-probebility[1]/(probebility[2]+probebility[1])
probebility

